import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Platform } from 'react-native'
import { getMetricMetaInfo, timeToString } from "../utils/helpers";
import NativeFitnessSlider from "./NativeFitnessSlider";
import NativeFitnessSteppers from "./NativeFitnessSteppers";
import DateHeader from "./DateHeader";
import RenderLog from "./RenderLog"
import { Ionicons } from '@expo/vector-icons';
import TextButton from './TextButton';
import { submitEntry, removeEntry } from "../utils/api";
import { purple, white } from '../utils/colors';

function SubmitButton ({ onPress }){ // we pass in the this.submit
  return (
      <TouchableOpacity
        style={Platform.OS === 'ios'? styles.iosSubmitBtn : styles.AndroidSubmitBtn}
        onPress = {onPress} >
        <Text style={styles.submitBtnText}>Submit</Text>
      </TouchableOpacity>
  )
}

export default class AddEntry extends Component {
  state = {
    run: 0,
    bike: 0,
    swim: 0,
    sleep: 0,
    eat: 0,
  };

  increment = (metric) => {
    const { max, step } = getMetricMetaInfo(metric);
    this.setState((state)=>{
      const count = state[metric] + step; // increment up by 1

       return { // if it's already max then set it as max otherwise set it as count
        ...state,
        [metric]: count > max
            ? max
            : count
       }
    });

  };

  decrement = (metric) => {
    const { min, step } = getMetricMetaInfo(metric);

    this.setState((state)=>{
      const count = state[metric] - step; // increment up by 1

       return { // if it's already min then set it as min otherwise set it as count
        ...state,
        [metric]: count < min
            ? min
            : count
       }
    });

  };

  slide = (metric, value) => {
    this.setState (() => ({
      [metric]: value,
    }))
  };

  submit = () => {
    const key = timeToString(); // formatted date time for our calender
    const entry = this.state;
    this.setState(() => ({
        run: 0,
        bike: 0,
        swim: 0,
        sleep: 0,
        eat: 0,
    })); // when the user clicks submit the entry should be set back to the default state
    // TODO Update Redux

    // TODO Navigate to home

    // Save to 'DB'
    submitEntry({ key, entry})
    // TODO Clear local notification

  };

  reset = () => {
    const key = timeToString();
    // TODO update redux
    // TODO route to home
    // update DB
    removeEntry(key);
  };

  render() {
    // if there's no param given then it will return info on everything
    const metaInfo = getMetricMetaInfo();

    const { alreadyLogged } = this.props;

    if (alreadyLogged){
      return (
          <View>
          <Ionicons
            name={'ios-happy-outline'}
            size={100}
          />
          <Text>You already logged your information for today.</Text>
          <TextButton onPress={this.reset}>
            Reset
          </TextButton>
          </View>

      )
    }


    return (
      <View>
      <Text>{`\n`}</Text><Text>{`\n`}</Text>
        <DateHeader date = {(new Date()).toLocaleDateString()}/>
          { Object.keys(metaInfo).map((key) => {
            const { getIcon, type, ...rest } = metaInfo[key];
            const value = this.state[key];

            return (
                <View key = {key}>
                  { getIcon() }
                  { type === 'slider'
                    ? <NativeFitnessSlider
                        value = {value}
                        onChange = {(value) => this.slide(key,value)} />
                    : <NativeFitnessSteppers
                        value = {value}
                        onIncrement = {() => this.increment(key)}
                        onDecrement = {() => this.decrement(key)}
                      />
                  }

                </View>
            )
          })}
          <SubmitButton onPress={this.submit}/>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: white
  },
  row: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  iosSubmitBtn: {
    backgroundColor: purple,
    padding: 10,
    borderRadius: 7,
    height: 45,
    marginLeft: 40,
    marginRight: 40,
  },
  AndroidSubmitBtn: {
    backgroundColor: purple,
    padding: 10,
    paddingLeft: 30,
    paddingRight: 30,
    height: 45,
    borderRadius: 2,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
  },
  submitBtnText: {
    color: white,
    fontSize: 22,
    textAlign: 'center',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
})

